alive = true
amount = 0

function init()
	fraud = LoadLoop("LEVEL/fraud.ogg")
	idk = LoadLoop("LEVEL/idk.ogg")
	joeHead = FindShape("head")
	joeBody = FindBody("joe")
end

function say_fraud()
	if amount > 14 then return end
	if alive then 
		PlayLoop(fraud, joePos, 0.7)
	end
end

function say_idk()
	if amount < 14 then return end
	if amount > 25 then return end
	if alive then 
		PlayLoop(idk, joePos, 0.7) 
	end
end

function tick(dt)
	if not alive then return end
	local shapes = GetBodyShapes(joeBody)
	local joeBody = joeBody
	if IsBodyBroken(joeBody) then
		alive = false
		DebugPrint("dead")
	end
	local joePos = GetShapeWorldTransform(joeHead).pos
	local sub = VecSub(GetPlayerTransform().pos, joePos)
	local length = VecLength(sub)

	if length > 3 then return end
	amount=amount+dt
	say_fraud()
	say_idk()

end