


function init()
	strobe = FindShape("topstrobe", true)
	light = FindLight("topstrobe", true)
	fl_red, fl_orange, fr_red, fr_orange = FindShape("fl_red", true), FindShape("fl_orange", true), FindShape("fr_red", true), FindShape("fr_orange", true)
	bl_red, bl_orange, br_red, br_orange = FindShape("bl_red", true), FindShape("bl_orange", true), FindShape("br_red", true), FindShape("br_orange", true)
	rightBusDoor = FindJoint("rightBusDoor",true)
	leftBusDoor = FindJoint("leftBusDoor",true)
	rearBusDoor = FindJoint("rearBusDoor",true)
	limitMinR, limitMaxR = GetJointLimits(rightBusDoor)
	limitMinL, limitMaxL = GetJointLimits(leftBusDoor)
	limitMinRear, limitMaxRear = GetJointLimits(rearBusDoor)
	doorSound = LoadSound("LEVEL/fdoorclose.ogg")
end
function print(s)
	SetString("hud.notification", tostring(s))
end
local tickTime = 0
local mode = "Flashing Red" --Flashing Orange, Not Flashing
function tick(dt)
	
	local vehicle = GetPlayerVehicle()
	if vehicle > 0 and HasTag(vehicle, "soundbus") then
		--[[ TOP STROBER ]]
		
		if tickTime < 20 then
			SetShapeEmissiveScale(strobe, 1)
			--SetLightEnabled(light, true)
			SetLightColor(light, 10, 10, 10)
		elseif tickTime < 30 then
			SetShapeEmissiveScale(strobe, 0)
			--SetLightEnabled(light, true)
			SetLightColor(light, 1, 1, 1)
		elseif tickTime < 40 then
			SetShapeEmissiveScale(strobe, 1)
			SetLightColor(light, 10, 10, 10)
		elseif tickTime < 50 then
			SetShapeEmissiveScale(strobe, 0)
			--SetLightEnabled(light, true)
			SetLightColor(light, 1, 1, 1)
		end
		--[[if tickTime > 25 then
			SetShapeEmissiveScale(strobe, 0)
			--SetLightEnabled(light, false)
			SetLightColor(light, 0, 0, 0)
		end]]
		
		--[[ FLASHING LIGHTS]]
		if mode == "Flashing Red" then
			if tickTime < 100 then
				--SetShapeEmissiveScale(fl_orange, 1)
				--SetShapeEmissiveScale(fr_orange, 1)
				--SetShapeEmissiveScale(bl_orange, 1)
				--SetShapeEmissiveScale(br_orange, 1)
				--SetShapeEmissiveScale(fl_red, 1)
				--SetShapeEmissiveScale(bl_red, 1)
				--SetShapeEmissiveScale(fr_red, 1)
				--SetShapeEmissiveScale(br_red, 1)
			end
			if tickTime > 100 then
				--SetShapeEmissiveScale(fl_orange, 0)
				--SetShapeEmissiveScale(fr_orange, 0)
				--SetShapeEmissiveScale(bl_orange, 0)
				--SetShapeEmissiveScale(br_orange, 0)
				--SetShapeEmissiveScale(fl_red, 0)
				--SetShapeEmissiveScale(bl_red, 0)
				--SetShapeEmissiveScale(fr_red, 0)
				--SetShapeEmissiveScale(br_red, 0)
			end
		end

		--[[ KEEP DOORS CLOSED ]]
		if GetJointMovement(rightBusDoor ) > limitMinR and GetJointMovement(rightBusDoor ) < limitMaxR then
            SetJointMotor(rightBusDoor, 1.75)
		end
		if GetJointMovement(leftBusDoor ) > limitMinL and GetJointMovement(leftBusDoor ) < limitMaxL then
            SetJointMotor(leftBusDoor, -1.75)
		end
		if GetJointMovement(rearBusDoor ) > limitMinRear and GetJointMovement(rearBusDoor ) < limitMaxRear then
			SetJointMotor(rearBusDoor, 1.75)
		end
		if tickTime == 150 then
			tickTime = 0
		end
		tickTime = tickTime+1
	else -- Not in bus
		SetJointMotor(rightBusDoor, 0, 0)
		SetJointMotor(leftBusDoor, 0, 0)
		SetJointMotor(rearBusDoor, 0, 0)
	end
end
   