
ENEMY_VISION_DISTANCE=100
ENEMY_SPEED=5

function init()
	shape = FindShape("npc")
	body = GetShapeBody(shape)
	npcBody = FindShape("npcbody")
	npcHead = FindShape("npc")
	npcUpperArm1 = FindShape("npcUA1")
	npcUpperArm2 = FindShape("npcUA2")
	npcLowerArm1 = FindShape("npcLA1")
	npcLowerArm2 = FindShape("npcLA2")
	npcHand1 = FindShape("npcHand1")
	npcHand2 = FindShape("npcHand2")
	npcLowerBody = FindShape("npcLB")
	npcUpperLeg1 = FindShape("npcUL1")
	npcUpperLeg2 = FindShape("npcUL2")
	npcLowerLeg1 = FindShape("npcLL1")
	npcLowerLeg2 = FindShape("npcLL2")
	npcFoot1 = FindShape("npcFoot1")
	npcFoot2 = FindShape("npcFoot2")
	currentSpeed = ENEMY_SPEED
	
end

--check head and body status and return 0 if either is broken
function alive()
	local headBroken = IsShapeBroken(npcHead)
	local bodyBroken = IsShapeBroken(npcBody)
	
	return not headBroken and not bodyBroken
	
end

--function to check status of legs and return lower value with more leg damage
function canWalk()
	local injuryValue = ENEMY_SPEED
	
	if IsShapeBroken(npcUpperLeg1) then 
		injuryValue = injuryValue / 1.2
	end
	if IsShapeBroken(npcUpperLeg2) then 
		injuryValue = injuryValue / 1.2
	end
	if IsShapeBroken(npcLowerLeg1) then 
		injuryValue = injuryValue / 1.2
	end
	if IsShapeBroken(npcLowerLeg2) then 
		injuryValue = injuryValue / 1.2
	end
	if IsShapeBroken(npcFoot1) then 
		injuryValue = injuryValue / 1.2
	end
	if IsShapeBroken(npcFoot2) then 
		injuryValue = injuryValue / 1.2
	end

	return injuryValue

end

function canSeePlayer()
	local playerPos = GetPlayerPos()
	local enemyPos = GetBodyTransform(body).pos

	--Direction to player
	local dir = VecSub(playerPos, enemyPos)
	local dist = VecLength(dir)
	dir = VecNormalize(dir)
	QueryRejectBody(body)

	return dist < ENEMY_VISION_DISTANCE and not QueryRaycast(enemyPos, dir, dist, 0, true)
end

--function to keep head hieght at 1.4
function stand()
	local t = GetBodyTransform(body)
	QueryRejectShape(npcBody)
	QueryRejectShape(npcHead)
	QueryRejectShape(npcLowerBody)
	QueryRejectShape(npcUpperArm1)
	QueryRejectShape(npcUpperArm2)
	QueryRejectShape(npcLowerArm1)
	QueryRejectShape(npcLowerArm2)
	QueryRejectShape(npcHand1)
	QueryRejectShape(npcHand2)
	QueryRejectShape(npcUpperLeg1)
	QueryRejectShape(npcUpperLeg2)
	QueryRejectShape(npcLowerLeg1)
	QueryRejectShape(npcLowerLeg2)
	QueryRejectShape(npcFoot1)
	QueryRejectShape(npcFoot2)
	--check distance to ground with raycast
	local hit, d = QueryRaycast(t.pos, Vec(0, -1, 0), 5)
	if hit then
	   DebugPrint(d)
	   --if less than 1.4 then add transform upwards to stand
	   if d < 1.4 then 
	   t.pos = VecAdd(t.pos, Vec(0, 0.1, 0)) 
	   SetBodyTransform(body, t)
	   end
	end
	
end

function vecToLastKnownLocation()
	return VecSub(lastKnownLocation, GetBodyTransform(body).pos)
end

function vecToPlayer()
	return VecSub(GetPlayerPos(), GetBodyTransform(body).pos)
end

function move(dt)
	if canSeePlayer() or VecLength(vecToPlayer()) < 10 then
		memoryTimer=0
		lastKnownLocation = GetPlayerPos()
	end


	if lastKnownLocation == nil then
		return
	end

	local dirToPlayer = VecNormalize(vecToLastKnownLocation())
	SetBodyVelocity(body, VecScale(dirToPlayer, currentSpeed))

end

function tick(dt)
	--DebugPrint(currentSpeed)
    if not alive() then return end
	--reduce enemy speed more if more leg damage
	currentSpeed = canWalk()
	if canWalk() > 3 then stand() end
	move(dt)
end

